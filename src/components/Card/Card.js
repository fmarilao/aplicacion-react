import React from 'react'
import './Card.css'

const Card = ({ data }) => {
  return (
    <div className="Container">
        <p>Ciudad: {data.name}</p>
        <p>Clima actual: {Math.round(data.main.temp)}°</p>
        <p>maxima: {Math.round(data.main.temp_max)}°</p>
        <p>minima: {Math.round(data.main.temp_min)}°</p>
        <p>sensacion termica: {Math.round(data.main.feels_like)}°</p>
    </div>
  )
}

export default Card