import React, { useState, useEffect } from 'react'
import Card from '../Card/Card'

// clave con la que accedo a la API
let apiKey='16569b100b8838ac574eb8428b6c5de8'

const Main = () => {
  // estados locales
  const [query, setQuery] = useState(''); // escucha al input
  const [actualWeather, setActualWeather] = useState()  // se completa con la rta de api
  const [showError, setShowError] = useState(null) // estado para mostrar errores

  // funcion que va a llamar a la API, con la ciudad que reciba por prop
  const getWeatherData = (city) => {
    fetch(`http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}&units=metric`)
    .then(r => r.json())
    .then((resp)=> {
      // si no encuentra la ciudad respondiendo con 404
      if (resp.cod === '404') {
        // muestro un error usando el estado
        setShowError(resp.message)
      } else {
        // guardamos en el estado local
        setActualWeather(resp)
      }
    })
    .catch((e) => {
      console.error(e)
    })
  }

  // funcion se ejecuta al apretar boton Agregar
  const searchNewCity = () => {
    if (query) {
      // siempre limpio los errores al buscar algo nuevo
      // esperando que el imput completo y la llamada sea satisfactoria 
      setShowError(null)
      // guardo en el local storage
      localStorage.setItem('myCityWeather', query);
      // hago el pedido a la api
      getWeatherData(query);
    } else {
      // muestro el error en caso que el input este vacio
      setShowError('Ingrese una busqueda')
    }
  }
  
  useEffect(() => {
    // logica al montar el componente o cambie algun valor
    // de alguna propiedad del array de dependencia
    // console.log('carga useEffect')
    // guardo lo que hay en el localstorage en una const
    const savedCity = localStorage.getItem('myCityWeather') 
    // console.log('saved city', savedCity)
    // si existe, llamo automaticamente a la API para que consulte los datos actuales
    if (savedCity) {
      // console.log('llamo a la func getWeatherData con la city del localstorage', savedCity)
      getWeatherData(savedCity)
    }
    return () => {
      //logica al desmontar un componente <<-- no lo usamos en este caso
    }
  }, [])

  return (
    <div>
        <h1>Aplicacion de clima</h1>
        <h3>utilizando localStorage</h3>
        <input
          placeholder='Ciudad...' 
          value={query} 
          onChange={(e) => setQuery(e.target.value)}
        />
        {showError ? <p>{showError}</p> : null}
        <button type='button' onClick={() => searchNewCity()}>Agregar</button>
        <button type='button' onClick={() => setQuery('')}>Limpiar</button>
        {actualWeather ? <Card data={actualWeather} /> : null}
        <br></br>
        <button
          type='button' 
          onClick={() => localStorage.removeItem('myCityWeather')}
          >
          Limpiar Local Storage
        </button>
    </div>
  )
}

export default Main